<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sha256;

        return [
            'brand_id' => Brand::factory(),
            'name' => $name,
            'slug' => Str::slug($name),
            'price' => $this->faker->numberBetween(5000, 500000),
            'description' => $this->faker->paragraph,
        ];
    }

    public function unitTest()
    {
        return $this->state(function (array $attributes) {
            return [
                'brand_id' => Brand::factory()->make(),
                'name' => $attributes['name'],
                'slug' => $attributes['slug'],
                'price' => $attributes['price'],
                'description' => $attributes['description'],
            ];
        });
    }
}
