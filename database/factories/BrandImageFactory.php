<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\BrandImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrandImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BrandImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'brand_id' => Brand::factory(),
            'filename' => $this->faker->word,
        ];
    }
}
