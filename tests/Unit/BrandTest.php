<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\BrandImage;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * @coversNothing
 */
class BrandTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_brand_has_many_products()
    {
        $brand    = Brand::factory()->create();
        $products = Product::factory()->times(3)->create(['brand_id' => $brand->id]);

        $this->assertEquals(3, $brand->products->count());
        $this->assertInstanceOf(Collection::class, $brand->products);
        foreach ($brand->products as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    /** @test */
    public function a_product_can_have_an_images()
    {
        $brand = Brand::factory()->create();
        BrandImage::factory()->create(['brand_id' => $brand->id]);

        $this->assertInstanceOf(BrandImage::class, $brand->image);
    }
}
