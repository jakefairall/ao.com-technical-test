<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\BrandImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

/**
 * @coversNothing
 */
class BrandImageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_brand_image_belongs_to_a_brand()
    {
        $brandImage = BrandImage::factory()->create();

        $this->assertInstanceOf(Brand::class, $brandImage->brand);
    }

    /** @test */
    public function image_can_be_retrieved_from_s3()
    {
        Storage::fake('brand_images');

        $file = UploadedFile::fake()->image('brand-image.jpg');
        Storage::putFileAs('', $file, 'brand-image.jpg');

        $image = BrandImage::factory()->create(['filename' => 'brand-image.jpg']);

        Storage::assertExists($image->filename);
        $this->assertEquals('/storage/brand-image.jpg', Storage::disk('brand_images')->url($image->filename));
    }

    /** @test */
    public function can_retrieve_brand_image_url()
    {
        Storage::fake('brand_images');

        $file = UploadedFile::fake()->image('brand-image.jpg');
        Storage::putFileAs('', $file, 'brand-image.jpg');

        $image = BrandImage::factory()->create(['filename' => 'brand-image.jpg']);

        Storage::assertExists($image->filename);
        $this->assertEquals('/storage/brand-image.jpg', $image->url);
    }
}
