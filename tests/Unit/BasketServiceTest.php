<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Services\BasketService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class BasketServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_retrieve_the_basket_from_session()
    {
        $product = Product::factory()->create();

        session(['basket' => collect([$product->id => collect(['product' => $product, 'quantity' => 1])])]);

        $basket = BasketService::getBasket();

        $this->assertEquals(collect([
            $product->id => collect([
                'product'  => $product,
                'quantity' => 1,
            ]),
        ]), $basket);
    }

    /** @test */
    public function can_update_the_basket()
    {
        $product = Product::factory()->create();

        $basket = BasketService::setBasket(
            collect([
                $product->id => collect([
                    'name'          => $product->name,
                    'slug'          => $product->slug,
                    'quantity'      => 1,
                    'unit_price'    => $product->price,
                    'decimal_price' => $product->decimal_price,
                    'total_price'   => number_format(($product->price * 1) / 100, 2),
                    'image_url'     => 'placeholder.jpg',
                ]),
            ])
        );

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), $basket);
    }

    /** @test */
    public function can_add_a_product_to_the_basket()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function adding_a_second_instance_of_a_product_to_the_basket_increases_the_quantity()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);
        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 2,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 2) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function can_increase_the_quantity_of_a_product_in_the_basket()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);
        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 2,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 2) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));

        BasketService::setQuantity($product, 3);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 3,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 3) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function can_reduce_the_quantity_of_a_product_in_the_basket()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);
        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 2,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 2) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));

        BasketService::setQuantity($product, 1);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function the_quantity_of_a_product_cannot_be_set_to_less_than_one()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));

        BasketService::setQuantity($product, 0);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function can_remove_a_product_from_the_basket()
    {
        $product = Product::factory()->create();

        BasketService::addProduct($product);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));

        BasketService::removeProduct($product);

        $this->assertEmpty(session('basket'));
    }

    /** @test */
    public function can_calculate_the_total_price_of_the_basket()
    {
        $productOne = Product::factory()->create(['price' => 79900]);
        $productTwo = Product::factory()->create(['price' => 59200]);

        BasketService::addProduct($productOne);
        BasketService::addProduct($productTwo);

        $this->assertEquals('1,391.00', BasketService::calculateTotal(BasketService::getBasket()));
    }

    /** @test */
    public function can_calculate_the_total_price_of_a_product()
    {
        $product = Product::factory()->create(['price' => 79900]);

        $this->assertEquals('1,598.00', BasketService::calculateProductTotal($product->price, 2));
    }
}
