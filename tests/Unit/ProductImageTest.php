<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ProductImageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_image_belongs_to_a_brand()
    {
        $productImage = ProductImage::factory()->create();

        $this->assertInstanceOf(Product::class, $productImage->product);
    }

    /** @test */
    public function image_can_be_retrieved_from_s3()
    {
        Storage::fake('product_images');

        $file = UploadedFile::fake()->image('fake-image.jpg');
        Storage::putFileAs('', $file, 'fake-image.jpg');

        $image = ProductImage::factory()->create(['filename' => 'fake-image.jpg']);

        Storage::assertExists($image->filename);
        $this->assertEquals('/storage/fake-image.jpg', Storage::disk('product_images')->url($image->filename));
    }

    /** @test */
    public function can_retrieve_product_image_url()
    {
        Storage::fake('product_images');

        $file = UploadedFile::fake()->image('fake-image.jpg');
        Storage::putFileAs('', $file, 'fake-image.jpg');

        $image = ProductImage::factory()->create(['filename' => 'fake-image.jpg']);

        Storage::assertExists($image->filename);
        $this->assertEquals('/storage/fake-image.jpg', $image->url);
    }
}
