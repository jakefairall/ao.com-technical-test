<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductFeature;
use App\Models\ProductImage;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ProductTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_belongs_to_a_brand()
    {
        $brand   = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);

        $this->assertInstanceOf(Brand::class, $product->brand);
    }

    /** @test */
    public function a_product_can_have_multiple_images()
    {
        $product = Product::factory()->create();
        ProductImage::factory()->times(3)->create(['product_id' => $product->id]);

        $this->assertEquals(3, $product->images->count());
        $this->assertInstanceOf(Collection::class, $product->images);
        foreach ($product->images as $image) {
            $this->assertInstanceOf(ProductImage::class, $image);
        }
    }

    /** @test */
    public function a_product_can_have_multiple_features()
    {
        $product = Product::factory()->create();
        ProductFeature::factory()->times(3)->create(['product_id' => $product->id]);

        $this->assertEquals(3, $product->features->count());
        $this->assertInstanceOf(Collection::class, $product->features);
        foreach ($product->features as $feature) {
            $this->assertInstanceOf(ProductFeature::class, $feature);
        }
    }

    /** @test */
    public function can_get_decimal_price()
    {
        $product = Product::factory()->unitTest()->make(['price' => 79900]);

        $this->assertEquals('799.00', $product->decimal_price);
    }

    /** @test */
    public function the_slug_must_be_unique()
    {
        Product::factory()->create([
            'name' => 'iPhone 12',
            'slug' => Str::slug('iPhone 12'),
        ]);

        try {
            Product::factory()->create([
                'name' => 'iPhone 11',
                'slug' => Str::slug('iPhone 12'),
            ]);
        } catch (QueryException $e) {
            $this->assertStringContainsString(
                'Integrity constraint violation: 19 UNIQUE constraint failed: products.slug',
                $e->getMessage()
            );

            return true;
        }

        $this->fail('Exception was not thrown');
    }
}
