<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Models\ProductFeature;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ProductFeatureTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_product_feature_belongs_to_a_brand()
    {
        $productFeature = ProductFeature::factory()->create();

        $this->assertInstanceOf(Product::class, $productFeature->product);
    }
}
