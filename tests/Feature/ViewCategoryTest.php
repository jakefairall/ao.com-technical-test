<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ViewCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_a_category()
    {
        Storage::fake('product_images');

        $brand = Brand::create([
            'name'        => 'Apple',
            'slug'        => Str::slug('Apple'),
            'description' => 'Apple Inc. designs, manufactures and markets mobile communication and media devices, personal computers and portable digital music players. The Company sells a range of related software, services, accessories, networking solutions, and third-party digital content and applications.',
        ]);

        $productOne = Product::create([
            'brand_id'    => $brand->id,
            'name'        => 'iPhone 12',
            'slug'        => Str::slug('iPhone 12'),
            'price'       => 79900,
            'description' => "The iPhone 12 and iPhone 12 mini are Apple's mainstream flagship iPhones for 2020. The phones come in 6.1-inch and 5.4-inch sizes with identical features, including support for faster 5G cellular networks, OLED displays, improved cameras, and Apple's latest A14 chip, all in a completely refreshed design.",
        ]);

        $productOneImageFile = UploadedFile::fake()->image('iphone-one.jpg');
        Storage::putFileAs('', $productOneImageFile, 'iphone-one.jpg');
        $productOneImage = ProductImage::create([
            'product_id' => $productOne->id,
            'filename'   => 'iphone-one.jpg',
        ]);

        $productTwo = Product::create([
            'brand_id'    => $brand->id,
            'name'        => 'iPhone 11',
            'slug'        => Str::slug('iPhone 11'),
            'price'       => 50000,
            'description' => "The iPhone 11 is Apple's mainstream flagship iPhones for 2019.",
        ]);

        $productTwoImageFile = UploadedFile::fake()->image('iphone-one.jpg');
        Storage::putFileAs('', $productTwoImageFile, 'iphone-one.jpg');
        $productTwoImage = ProductImage::create([
            'product_id' => $productTwo->id,
            'filename'   => 'iphone-one.jpg',
        ]);

        $response = $this->get(route('category.index', $brand->slug));
        $response->assertStatus(200);
        $response->assertSee($brand->name);
        $response->assertSee($brand->description);
        $response->assertSee('Apple');
        $response->assertSee('iPhone 12');
        $response->assertSee("The iPhone 12 and iPhone 12 mini are Apple's mainstream flagship iPhones for 2020. The phones come in 6.1-inch and 5.4-inch sizes with identical features, including support for faster 5G cellular networks, OLED displays, improved cameras, and Apple's latest A14 chip, all in a completely refreshed design.");
        $response->assertSee('799.00');
        $response->assertSee($productTwoImage->url);
        $response->assertSee('iPhone 11');
        $response->assertSee("The iPhone 11 is Apple's mainstream flagship iPhones for 2019.");
        $response->assertSee('500.00');
        $response->assertSee($productTwoImage->url);
    }
}
