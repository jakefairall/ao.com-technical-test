<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductFeature;
use App\Models\ProductImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ViewProductListingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_a_product_listing()
    {
        Storage::fake('product_images');

        $brand = Brand::create([
            'name'        => 'Apple',
            'slug'        => Str::slug('Apple'),
            'description' => 'Apple Inc. designs, manufactures and markets mobile communication and media devices, personal computers and portable digital music players. The Company sells a range of related software, services, accessories, networking solutions, and third-party digital content and applications.',
        ]);

        $product = Product::create([
            'brand_id'    => $brand->id,
            'name'        => 'iPhone 12',
            'slug'        => Str::slug('iPhone 12'),
            'price'       => 79900,
            'description' => "The iPhone 12 and iPhone 12 mini are Apple's mainstream flagship iPhones for 2020. The phones come in 6.1-inch and 5.4-inch sizes with identical features, including support for faster 5G cellular networks, OLED displays, improved cameras, and Apple's latest A14 chip, all in a completely refreshed design.",
        ]);

        $productImageOneFile = UploadedFile::fake()->image('iphone-one.jpg');
        Storage::putFileAs('', $productImageOneFile, 'iphone-one.jpg');
        $productImageOne = ProductImage::create([
            'product_id' => $product->id,
            'filename'   => 'iphone-one.jpg',
        ]);

        $productImageTwoFile = UploadedFile::fake()->image('iphone-two.jpg');
        Storage::putFileAs('', $productImageTwoFile, 'iphone-two.jpg');
        $productImageTwo = ProductImage::create([
            'product_id' => $product->id,
            'filename'   => 'iphone-two.jpg',
        ]);

        $productFeatureOne = ProductFeature::create([
            'product_id'  => $product->id,
            'title'       => 'Display',
            'description' => '6.1-inch Super Retina XDR display',
        ]);
        $productFeatureTwo = ProductFeature::create([
            'product_id'  => $product->id,
            'title'       => 'Strength',
            'description' => 'Ceramic Shield, tougher than any smartphone glass',
        ]);
        $productFeatureThree = ProductFeature::create([
            'product_id'  => $product->id,
            'title'       => '5G Capable',
            'description' => '5G for superfast downloads and high-quality streaming',
        ]);

        $response = $this->get(route('products.show', $product->slug));
        $response->assertStatus(200);
        $response->assertSee('Apple');
        $response->assertSee('iPhone 12');
        $response->assertSee('799.00');
        $response->assertSee("The iPhone 12 and iPhone 12 mini are Apple's mainstream flagship iPhones for 2020. The phones come in 6.1-inch and 5.4-inch sizes with identical features, including support for faster 5G cellular networks, OLED displays, improved cameras, and Apple's latest A14 chip, all in a completely refreshed design.");
        $response->assertSee($productImageOne->url);
        $response->assertSee($productImageTwo->url);
        $response->assertSee($productFeatureOne->title);
        $response->assertSee($productFeatureOne->description);
        $response->assertSee($productFeatureTwo->title);
        $response->assertSee($productFeatureTwo->description);
        $response->assertSee($productFeatureThree->title);
        $response->assertSee($productFeatureThree->description);
    }
}
