<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\ProductImage;
use App\Services\BasketService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ViewBasketTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_an_empty_basket()
    {
        $response = $this->get(route('basket.index'));
        $response->assertStatus(200);
        $response->assertSee('You have no items in your basket.');
    }

    /** @test */
    public function user_can_view_a_single_item_in_their_basket()
    {
        $product = Product::factory()->create();
        ProductImage::factory()->create(['product_id' => $product->id]);
        BasketService::addProduct($product);

        $product = Product::factory()->create();
        ProductImage::factory()->create(['product_id' => $product->id]);
        BasketService::addProduct($product);

        $response = $this->get(route('basket.index'));
        $response->assertStatus(200);
        $response->assertSee($product->name);
        $response->assertSee($product->images->first()->url);
        $response->assertSee($product->decimal_price);
        $response->assertSee('Total');
        $response->assertSee(BasketService::calculateTotal(BasketService::getBasket()));
    }
}
