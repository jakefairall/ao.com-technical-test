<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Services\BasketService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class UpdateBasketQuantityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_update_the_quantity_of_a_product_in_their_basket()
    {
        $product = Product::factory()->create()->fresh();

        BasketService::addProduct($product);

        $response = $this->json('post', route('basket.update'), [
            'product'  => $product->slug,
            'quantity' => 5,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'product_total' => BasketService::calculateProductTotal($product->price, 5),
            'total'         => BasketService::calculateTotal(BasketService::getBasket()),
        ]);
    }

    /** @test */
    public function quantity_cannot_be_less_than_one()
    {
        $product = Product::factory()->create()->fresh();

        BasketService::addProduct($product);

        $response = $this->json('post', route('basket.update'), [
            'product'  => $product->slug,
            'quantity' => -1,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'quantity' => [
                    'The quantity must be at least 1.',
                ],
            ],
        ]);
    }

    /** @test */
    public function product_must_exist()
    {
        $response = $this->json('post', route('basket.update'), [
            'product'  => 1,
            'quantity' => 1,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'product' => [
                    'The selected product is invalid.',
                ],
            ],
        ]);
    }
}
