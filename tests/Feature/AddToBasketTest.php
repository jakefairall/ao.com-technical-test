<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class AddToBasketTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_add_a_product_to_their_basket()
    {
        $product = Product::factory()->create()->fresh();

        $response = $this->post(route('basket.store'), [
            'product' => $product->slug,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'The product was successfully added to your basket!',
        ]);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 1,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 1) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }

    /** @test */
    public function adding_a_product_to_the_basket_again_increases_the_quantity()
    {
        $product = Product::factory()->create()->fresh();

        $this->post(route('basket.store'), [
            'product' => $product->slug,
        ]);
        $response = $this->post(route('basket.store'), [
            'product' => $product->slug,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'The product was successfully added to your basket!',
        ]);

        $this->assertEquals(collect([
            $product->id => collect([
                'name'          => $product->name,
                'slug'          => $product->slug,
                'quantity'      => 2,
                'unit_price'    => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price'   => number_format(($product->price * 2) / 100, 2),
                'image_url'     => 'placeholder.jpg',
            ]),
        ]), session('basket'));
    }
}
