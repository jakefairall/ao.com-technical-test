<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\BrandImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

/**
 * @coversNothing
 */
class ViewHomePageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_the_homepage()
    {
        Storage::fake('brand_images');

        $brandOne = Brand::create([
            'name'        => 'Apple',
            'slug'        => 'apple',
            'description' => 'Apple Inc. designs, manufactures and markets mobile communication and media devices, personal computers and portable digital music players. The Company sells a range of related software, services, accessories, networking solutions, and third-party digital content and applications.',
        ]);
        $brandOneImageFile = UploadedFile::fake()->image('apple.jpg');
        Storage::putFileAs('', $brandOneImageFile, 'apple.jpg');
        $brandOneImage = BrandImage::create([
            'brand_id' => $brandOne->id,
            'filename' => 'apple.jpg',
        ]);

        $brandTwo = Brand::create([
            'name'        => 'Samsung',
            'slug'        => 'samsung',
            'description' => "Samsung, South Korean company that is one of the world's largest producers of electronic devices. Samsung specializes in the production of a wide variety of consumer and industry electronics, including appliances, digital media devices, semiconductors, memory chips, and integrated systems.",
        ]);
        $brandTwoImageFile = UploadedFile::fake()->image('samsung.jpg');
        Storage::putFileAs('', $brandTwoImageFile, 'samsung.jpg');
        $brandTwoImage = BrandImage::create([
            'brand_id' => $brandTwo->id,
            'filename' => 'samsung.jpg',
        ]);

        $response = $this->get(route('home.index'));
        $response->assertStatus(200);
        $response->assertSee($brandOne->name);
        $response->assertSee($brandOne->description);
        $response->assertSee($brandOneImage->url);
        $response->assertSee($brandTwo->name);
        $response->assertSee($brandTwo->description);
        $response->assertSee($brandTwoImage->url);
    }
}
