<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Services\BasketService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @coversNothing
 */
class RemoveFromBasketTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_remove_a_product_to_their_basket()
    {
        $product = Product::factory()->create()->fresh();

        BasketService::addProduct($product);

        $response = $this->json('delete', route('basket.destroy'), [
            'product' => $product->slug,
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'total' => BasketService::calculateTotal(BasketService::getBasket()),
        ]);

        $this->assertEquals(collect([]), session('basket'));
    }

    /** @test */
    public function product_must_exist()
    {
        $response = $this->json('delete', route('basket.destroy'), [
            'product' => 1,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'product' => [
                    'The selected product is invalid.',
                ],
            ],
        ]);
    }
}
