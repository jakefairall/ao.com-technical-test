<?php

use App\Http\Controllers\BasketController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::get('/products/{product}', [ProductController::class, 'show'])->name('products.show');

Route::get('/basket', [BasketController::class, 'index'])->name('basket.index');
Route::post('/basket/add', [BasketController::class, 'store'])->name('basket.store');
Route::post('/basket/update', [BasketController::class, 'update'])->name('basket.update');
Route::delete('/basket', [BasketController::class, 'destroy'])->name('basket.destroy');

Route::get('/category/{brand}', [BrandController::class, 'index'])->name('category.index');
