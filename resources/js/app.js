require('./bootstrap');

$(document).ready(function() {
    $('#add-to-cart').click(function () {
        let button = $(this);
        button.prop('disabled', true);
        button.addClass('opacity-50 cursor-not-allowed');
        button.removeClass('hover:bg-indigo-500 active:bg-indigo-700');

        window.axios.post(button.data('url'), {
            product: button.data('product')
        }).then(function (response) {
            updateAlert('green', response.data.message);
        }).catch(function (error) {
            updateAlert('red', error.response.data.message);
        }).then(function () {
            button.prop('disabled', false);
            button.removeClass('opacity-50 cursor-not-allowed');
            button.addClass('hover:bg-indigo-500 active:bg-indigo-700');
        });
    });

    $("input[type='number']").change(function () {
        let product = $(this).data('product');

        window.axios.post($(this).data('url'), {
            product: product,
            quantity: $(this).val()
        }).then(function (response) {
            console.log(response);
            $(`#item_total_${product}`).html(response.data.product_total);
            $('#total').html(response.data.total);
        }).catch(function (error) {

        }).then(function () {

        });
    });

    $(".delete").click(function () {
        let product = $(this).data('product');

        window.axios.delete($(this).data('url'), {
            data: {
                product: product,
            }
        }).then(function (response) {
            $(`#${product}`).remove();
            $('#total').html(response.data.total);

            console.log($('tbody tr').length);
            if($('tbody tr').length == 1) {
                $('#no-items').css('display', 'block')
                $('.basket').css('display', 'none');
            }

        }).catch(function (error) {

        }).then(function () {

        });
    })

    $('#close-alert').click(function() {
        $('#alert').css('display', 'none');
    })

    function updateAlert(replace, message)
    {
        let remove = 'green';
        let alert = $('#alert');
        let svgSuccess = $('#alert-svg-success');
        let svgFailure = $('#alert-svg-failure');

        if(replace === 'green') {
            remove = 'red';
            svgFailure.css('display', 'none');
            svgSuccess.css('display', '');
        } else {
            svgSuccess.css('display', 'none');
            svgFailure.css('display', '');
        }

        alert.children().removeClass(`bg-${remove}-50`).addClass(`bg-${replace}-50`);
        alert.find('p').html(message).removeClass(`text-${remove}-800`).addClass(`text-${replace}-800`);
        alert.find('button').removeClass(`text-${remove}-500 hover:bg-${remove}-100 focus:bg-${remove}-100`).addClass(`text-${replace}-500 hover:bg-${replace}-100 focus:bg-${replace}-100`);
        alert.css('display', '');
    }
});
