@extends('layout')

@section('content')
    <div class="relative pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
        <div class="absolute inset-0">
            <div class="bg-white h-1/3 sm:h-2/3"></div>
        </div>
        <div class="relative max-w-7xl mx-auto">
            <div class="text-center">
                <h2 class="text-3xl leading-9 tracking-tight font-extrabold text-gray-900 sm:text-4xl sm:leading-10">
                    {{ $brand->name }}
                </h2>
                <p class="mt-3 max-w-2xl mx-auto text-xl leading-7 text-gray-500 sm:mt-4">
                    {{ $brand->description }}
                </p>
            </div>
            <div class="mt-12 grid gap-5 max-w-lg mx-auto lg:grid-cols-3 lg:max-w-none">
                @foreach($products as $product)
                    <div class="flex flex-col rounded-lg shadow-lg overflow-hidden">
                        <div class="flex-shrink-0">
                            <a href="{{ route('products.show', $product->slug) }}">
                                <img class="h-64 w-full object-cover" src="{{ $product->images->first()->url }}" alt="">
                            </a>
                        </div>
                        <div class="flex-1 bg-white p-6 flex flex-col justify-between">
                            <div class="flex-1">
                                <a href="{{ route('products.show', $product->slug) }}" class="block hover:underline">
                                    <h3 class="mt-2 text-xl leading-7 font-semibold text-gray-900">
                                        {{ $product->name }}
                                    </h3>
                                </a>
                                <a href="{{ route('products.show', $product->slug) }}">
                                    <p class="mt-3 text-base leading-6 text-gray-500">
                                        {{ $product->description }}
                                    </p>
                                </a>
                            </div>
                            <div class="mt-6 flex items-center">
                                <div>
                                    <a href="{{ route('products.show', $product->slug) }}">
                                        <p class="text-lg leading-5 font-medium text-gray-900">
                                            &pound;{{ $product->decimal_price }}
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection()
