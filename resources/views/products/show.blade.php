@extends('layout')

@section('content')
    <div class="bg-white">
        <div id="alert" style="display: none;">
            <div class="rounded-md bg-green-50 p-4 mt-16">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <svg id="alert-svg-success" style="display:none;" class="h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                        </svg>
                        <svg id="alert-svg-failure" style="display: none;" class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <p class="text-sm leading-5 font-medium text-green-800"></p>
                    </div>
                    <div class="ml-auto pl-3">
                        <div class="-mx-1.5 -my-1.5">
                            <button id="close-alert" class="inline-flex rounded-md p-1.5 text-green-500 hover:bg-green-100 focus:outline-none focus:bg-green-100 transition ease-in-out duration-150" aria-label="Dismiss">
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="max-w-screen-xl mx-auto py-16 lg:py-16 lg:grid lg:grid-cols-3 lg:gap-x-8">
            <div class="lg:col-span-2">
                <h2 class="text-base leading-6 font-semibold text-indigo-600 uppercase tracking-wide">{{ $product->brand->name }}</h2>
                <p class="mt-2 text-3xl leading-9 font-extrabold text-gray-900">{{ $product->name }}</p>
                <p class="mt-4 text-lg leading-7 text-gray-500">{{ $product->description }}</p>
                <p class="mt-4 text-lg leading-7 text-gray-800">&pound;{{ $product->decimal_price }}</p>
                <span class="inline-flex rounded-md shadow-sm lg:mt-5">
                    <button type="button" id="add-to-cart" data-url="{{ route('basket.store') }}" data-product="{{ $product->slug }}" class="inline-flex items-center px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                    <svg fill="none" viewBox="0 0 24 24" stroke="currentColor" id="shopping-cart" class="-ml-1 mr-3 h-5 w-5">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                    </svg>
                    Add to Basket
                  </button>
                </span>
            </div>
            <div class="lg:col-span-1">
                @isset($product->images)
                    @foreach($product->images as $image)
                        <img style="max-height: 310px;" src="{{ $image->url }}"/>
                    @endforeach
                @endisset
            </div>
            <div class="mt-20 lg:mt-20 lg:col-span-3">
                <dl class="space-y-10 sm:space-y-0 sm:grid sm:grid-cols-2 sm:grid-rows-4 sm:grid-flow-col sm:gap-x-6 sm:gap-y-10 lg:gap-x-8 lg:grid-rows-2">
                    @isset($product->features)
                        @foreach($product->features as $feature)
                            <div class="flex space-x-3">
                                <svg class="flex-shrink-0 h-6 w-6 text-green-500" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M5 13l4 4L19 7"/>
                                </svg>
                                <div class="space-y-2">
                                    <dt class="text-lg leading-6 font-medium text-gray-900">{{ $feature->title }}</dt>
                                    <dd class="flex space-x-3 lg:py-0 lg:pb-4">
                                        <span
                                            class="text-base leading-6 text-gray-500">{{ $feature->description }}</span>
                                    </dd>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                </dl>
            </div>
        </div>
    </div>
@endsection()
