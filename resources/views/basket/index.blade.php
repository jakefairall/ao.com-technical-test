@extends('layout')

@section('content')
    <div class="bg-white">
        <div class="max-w-screen-xl mx-auto py-16 lg:py-16 lg:grid lg:grid-cols-3 lg:gap-x-8">
            <div class="lg:col-span-4">
                <h1 class="text-lg leading-6 font-medium text-gray-900">Basket</h1>
            </div>
            <div class="lg:col-span-4 basket" @if(!$basket->count()) style="display: none;" @endif>
                <div class="flex flex-col mt-6">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead>
                                    <tr>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Item
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Unit Price
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Quantity
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Total
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                    @foreach($basket as $item)
                                        @if(is_iterable($item))
                                            <tr id="{{ $item['slug'] }}">
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0 h-10 w-10">
                                                            <img class="h-10 w-10 rounded-full" src="{{ $item['image_url'] }}" alt="{{ $item['name'] }}">
                                                        </div>
                                                        <div class="ml-4">
                                                            <div class="text-sm leading-5 font-medium text-gray-900">
                                                                {{ $item['name'] }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <div class="text-sm leading-5 text-gray-900">&pound;{{ $item['decimal_price'] }}</div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="number" value="{{ $item['quantity'] }}" name="quantity" data-url={{ route('basket.update') }} data-product="{{ $item['slug'] }}" min="1" />
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                    <div class="text-sm leading-5 text-gray-900">&pound;<span id="item_total_{{ $item['slug'] }}">{{ $item['total_price'] }}</span></div>
                                                </td>
                                                <td class="pr-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                                    <button class="inline-flex rounded-md p-1.5 text-indigo-500 hover:bg-indigo-100 focus:outline-none focus:bg-indigo-100 transition ease-in-out duration-150 delete" data-product="{{ $item['slug'] }}" data-url="{{ route('basket.destroy') }}">
                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                                        </svg>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach()
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lg:col-span-3 basket" @if(!$basket->count()) style="display: none;" @endif></div>
            <div class="lg:col-span-1 mt-6 basket" @if(!$basket->count()) style="display: none;" @endif>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <tr>
                                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                                Total
                                            </td>
                                            <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                &pound;<span id="total">{{ $total }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="text-md leading-6 font-medium text-gray-900 mt-6" @if($basket->count()) style="display: none;" @endif id="no-items">You have no items in your basket.</h2>
        </div>
    </div>
@endsection()
