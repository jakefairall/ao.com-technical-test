<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

        <link href="/css/app.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="/js/app.js"></script>
    </head>
    <body class="antialiased">
        @include('navigation')

        <div class="container mt-3 mx-auto px-4 sm:px-6 lg:px-8">
            @yield('content')
        </div>
    </body>
</html>
