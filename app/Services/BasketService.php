<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class BasketService
{
    public static function getBasket(): Collection
    {
        return session('basket') ?? collect();
    }

    public static function setBasket($basket): Collection
    {
        session(['basket_total_price' => self::calculateTotal($basket)]);
        session(['basket' => collect($basket)]);

        return session('basket');
    }

    public static function addProduct(Product $product): bool
    {
        $basket = self::getBasket();
        $quantity = 1;

        if (isset($basket[$product->id])) {
            $quantity = $basket[$product->id]['quantity'] + 1 ?? $quantity;
        }

        $basket->put($product->id, collect([
                'name' => $product->name,
                'slug' => $product->slug,
                'quantity' => $quantity,
                'unit_price' => $product->price,
                'decimal_price' => $product->decimal_price,
                'total_price' => self::calculateProductTotal($product->price, $quantity),
                'image_url' => $product->images->first()->url ?? 'placeholder.jpg',
            ])
        );

        self::setBasket($basket);

        return true;
    }

    public static function setQuantity(Product $product, int $quantity): bool
    {
        if ($quantity < 1) {
            return false;
        }

        $basket = self::getBasket();

        if (!isset($basket[$product->id])) {
            return true;
        }

        if ($quantity) {
            $basket[$product->id]['quantity'] = $quantity;
            $basket[$product->id]['total_price'] = self::calculateProductTotal($product->price, $quantity);

            self::setBasket($basket);
        }

        return true;
    }

    public static function removeProduct(Product $product): bool
    {
        $basket = self::getBasket();

        if (!isset($basket[$product->id])) {
            return true;
        }

        unset($basket[$product->id]);

        self::setBasket($basket);

        return true;
    }

    public static function calculateTotal($basket): string
    {
        $total = $basket->sum(function ($item) {
            if (is_iterable($item)) {
                return $item['unit_price'] * $item['quantity'];
            }

            return false;
        });

        return number_format($total / 100, 2);
    }

    public static function calculateProductTotal($price, $quantity): string
    {
        return number_format(($price * $quantity) / 100, 2);
    }
}
