<?php

namespace App\Http\Controllers;

use App\Models\Brand;

class BrandController extends Controller
{
    public function index(Brand $brand)
    {
        return view('brands.index', ['brand' => $brand, 'products' => $brand->products]);
    }
}
