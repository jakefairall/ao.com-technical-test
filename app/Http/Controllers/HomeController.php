<?php

namespace App\Http\Controllers;

use App\Models\Brand;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index', ['brands' => Brand::all()]);
    }
}
