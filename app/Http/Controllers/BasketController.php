<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyBasketRequest;
use App\Http\Requests\UpdateBasketRequest;
use App\Models\Product;
use App\Services\BasketService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BasketController extends Controller
{
    public function index()
    {
        return view('basket.index', ['basket' => BasketService::getBasket(), 'total' => session('basket_total_price')]);
    }

    public function store(Request $request)
    {
        $product = Product::where('slug', $request->get('product'))->first();

        if (BasketService::addProduct($product)) {
            return new Response(['message' => 'The product was successfully added to your basket!']);
        }
    }

    public function update(UpdateBasketRequest $request)
    {
        $product = Product::where('slug', $request->get('product'))->first();

        if (BasketService::setQuantity($product, $request->get('quantity'))) {
            return new Response([
                'product_total' => BasketService::calculateProductTotal($product->price, $request->get('quantity')),
                'total'         => BasketService::calculateTotal(BasketService::getBasket()),
            ]);
        }
    }

    public function destroy(DestroyBasketRequest $request)
    {
        $product = Product::where('slug', $request->get('product'))->first();

        if (BasketService::removeProduct($product)) {
            return new Response([
                'total' => BasketService::calculateTotal(BasketService::getBasket()),
            ]);
        }
    }
}
